package by.training.elevator;

import static org.junit.Assert.*;

import org.junit.Test;
import by.training.elevator.container.Building;
import by.training.elevator.container.Stories;

public class TestTransportationValidator {

    private static final String MESSAGE = "message";

    private Building someBuilding;
    private Stories someStories;

    @Test
    public void testValidateWhenGetPassengersNumberDispatchStoriesZero() {
        final int STORIES_NUMBER = 5;
        final int PASSENGERS_NUMBER = 10;
        final int ELEVATOR_CAPACITY = 2;
        final int ANIMATIN_BOOST = 0;
        someBuilding = new Building(STORIES_NUMBER, ELEVATOR_CAPACITY, PASSENGERS_NUMBER, ANIMATIN_BOOST);
        someStories = someBuilding.getStories();
        assertEquals(PASSENGERS_NUMBER, someStories.getPassengersNumberDispatchStories());
        assertEquals(0, someStories.getPassengersNumberElevatorContainer());
    }

    @Test
    public void testGetValidationWhenValidationTrue() {
        assertEquals(MESSAGE + Constants.MESSAGE_SUCCESS, TransportationValidator.getValidation(MESSAGE, true));
    }

    @Test
    public void testGetValidation_WhenValidationFalse() {
        assertEquals(MESSAGE + Constants.MESSAGE_FAILURE, TransportationValidator.getValidation(MESSAGE, false));
    }

}
