package by.training.elevator;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.hasItems;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import by.training.elevator.beans.Passenger;
import by.training.elevator.container.Building;
import by.training.elevator.container.Stories;
import by.training.elevator.container.Story;
import by.training.elevator.ifaces.IStories;

public class TestElevatorController {

    private static final int STORIES_NUMBER = 5;
    private static final int PASSENGERS_NUMBER = 20;
    private static final int ELEVATOR_CAPACITY = 2;
    private static final int ANIMATIN_BOOST = 0;

    private Building someBuilding;
    private ElevatorController someElevatorController;
    private Stories someStories;

    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    @Mock
    private IStories storiesMock;

    @Before
    public void setUp() throws Exception {
        someBuilding = new Building(STORIES_NUMBER, ELEVATOR_CAPACITY, PASSENGERS_NUMBER, ANIMATIN_BOOST);
        someElevatorController = someBuilding.getElevatorController();
        someStories = someBuilding.getStories();
    }

    @Test
    public void testInitTransportationTasksForEachPassengerTransportationTask() {
        Passenger[] passengersInStories = new Passenger[someStories.getPassengersNumber()];
        List<Passenger> passengersWithTask = new ArrayList<>();
        Set<TransportationTask> tasks = someElevatorController.getTasks();
        int currrentNumberPassenger = 0;
        for (int i = 1; i <= someStories.getStoriesNumber(); i++) {
            Story currentStory = someStories.getStory(i);
            Iterator<Passenger> passengers = currentStory.getDispatchStoryContainer().getIterator();
            while(passengers.hasNext()){            
                Passenger passenger = passengers.next();
                passengersInStories[currrentNumberPassenger] = passenger;
                currrentNumberPassenger++;
                if (tasks.contains(passenger.getTask())) {
                    passengersWithTask.add(passenger);
                }
            }
        }
        assertThat(passengersWithTask, hasItems(passengersInStories));
    }

    @Test
    public void getNumberCurrentStory() {        
        ElevatorController controller = new ElevatorController();
        controller.setStories(storiesMock);
        context.checking(new Expectations() {
            {
                oneOf(storiesMock).getNumberCurentStory();
            }
        });
        controller.getNumberCurrentStory();
    }

}
