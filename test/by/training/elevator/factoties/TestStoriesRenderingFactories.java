package by.training.elevator.factoties;

import static org.junit.Assert.*;

import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import by.training.elevator.Constants;
import by.training.elevator.ElevatorController;
import by.training.elevator.container.Building;
import by.training.elevator.container.Stories;
import by.training.elevator.container.StoriesRendering;
import by.training.elevator.container.StoriesRenderingConsole;
import by.training.elevator.ifaces.IStories;
import by.training.elevator.ifaces.IStoriesRendering;

public class TestStoriesRenderingFactories {
      
    private Building someBuilding;
    private Stories someStories;
    
    @Before
    public void setUp() throws Exception {
        final int STORIES_NUMBER = 5;
        final int PASSENGERS_NUMBER = 20;
        final int ELEVATOR_CAPACITY = 2;
        final int ANIMATION_BOOST = 10;   
        someBuilding = new Building(STORIES_NUMBER, ELEVATOR_CAPACITY, PASSENGERS_NUMBER, ANIMATION_BOOST);        
        someStories = someBuilding.getStories();
    }   
    
    @Test
    public void testGetStoriesRenderingWhenAnimationBoostNotZero() {
        Class clazz  = StoriesRendering.class;
        final int ANIMATION_BOOST_10 = 10;        
        assertEquals(clazz, StoriesRenderingFactories.getStoriesRendering(someStories, ANIMATION_BOOST_10).getClass());
    }
    
    @Test
    public void testGetStoriesRenderingWhenAnimationBoostZero() {          
        Class clazz  = StoriesRenderingConsole.class;
        final int ANIMATION_BOOST_0 = 0; 
        assertEquals(clazz, StoriesRenderingFactories.getStoriesRendering(someStories, ANIMATION_BOOST_0).getClass());
    }

}
