package by.training.elevator.container;

import static org.junit.Assert.*;

import org.junit.Test;

import by.training.elevator.beans.Passenger;

public class TestElevatorContainer {

    @Test
    public void testIsFull() {
        final int ELEVATOR_CAPACITY = 2;
        ElevatorContainer elevatorContainer = new ElevatorContainer(ELEVATOR_CAPACITY);
        
        elevatorContainer.addPassenger(new Passenger());        
        assertFalse(elevatorContainer.isFull());
        elevatorContainer.addPassenger(new Passenger());
        assertTrue(elevatorContainer.isFull());
    }

}
