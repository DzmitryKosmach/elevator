package by.training.elevator.container;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import by.training.elevator.TransportationState;
import by.training.elevator.beans.Passenger;

public class TestStories {

    private static final int STORIES_NUMBER = 5;
    private static final int PASSENGERS_NUMBER = 20;
    private static final int ELEVATOR_CAPACITY = 2;
    private Stories someStories;

    @Before
    public void setUp() throws Exception {
        someStories = new Stories(STORIES_NUMBER, PASSENGERS_NUMBER, ELEVATOR_CAPACITY);
    }

    @Test
    public void testGetCurrentSemaphoreWhenDirectionUpTrue() {
        assertEquals(someStories.getCurrentStory().getSemaphoreUp(), someStories.getCurrentSemaphore());
    }

    @Test
    public void testGetCurrentSemaphoreWhenDirectionUpFalse() {
        for (int i = 0; i <= (STORIES_NUMBER); i++) {
            someStories.nextStory();
        }
        assertEquals(someStories.getCurrentStory().getSemaphoreDown(), someStories.getCurrentSemaphore());
    }

    @Test
    public void testnextStory_MoveUpDown() {
        for (int i = 1; i < (STORIES_NUMBER - 1 + STORIES_NUMBER - 1); i++) {
            someStories.nextStory();
        }
        assertEquals(false, someStories.nextStory());
    }

    @Test
    public void testGetNextStoryWhenDirectionUpTrue() {
        int currentStory = someStories.getNumberCurentStory();
        assertEquals(currentStory + 1, someStories.getNextStory());
    }

    @Test
    public void testGetNextStoryWhenDirectionUpFalse() {
        for (int i = 1; i < STORIES_NUMBER; i++) {
            someStories.nextStory();
        }
        int currentStory = someStories.getNumberCurentStory();
        assertEquals(currentStory - 1, someStories.getNextStory());
    }

    @Test
    public void testBoadPassengerElevatorWhenElevatorNotFull() {
        Passenger passenger = new Passenger();
        someStories.getCurentDispatchStory().addPassenger(passenger);
        someStories.boadPassengerElevator(passenger);
        assertEquals(1, someStories.getElevatorContainer().getSize());
    }

    @Test
    public void testBoadPassengerElevatorWhenElevatorFull() {
        ElevatorContainer elevator = someStories.getElevatorContainer();
        for (int i = 1; i <= ELEVATOR_CAPACITY; i++) {
            elevator.addPassenger(new Passenger());
        }
        someStories.boadPassengerElevator(new Passenger());
        assertEquals(ELEVATOR_CAPACITY, elevator.getSize());
    }

    @Test
    public void testDeboadPassengerElevatorWhenElevatorContainsPassenger() {
        Passenger passenger = new Passenger();
        someStories.getCurentDispatchStory().addPassenger(passenger);
        someStories.boadPassengerElevator(passenger);
        someStories.deboadPassengerElevator(passenger);
        assertEquals(0, someStories.getElevatorContainer().getSize());
    }

    @Test
    public void testDeboadPassengerElevatorWhenElevatorContainsNotPassenger() {
        Passenger passenger = new Passenger();
        someStories.getCurentDispatchStory().addPassenger(passenger);
        someStories.boadPassengerElevator(passenger);
        Passenger otherPassenger = new Passenger();
        otherPassenger.setState(TransportationState.ABORTED);
        someStories.deboadPassengerElevator(otherPassenger);
        assertEquals(1, someStories.getElevatorContainer().getSize());
    }

}
