package by.training.elevator;

import by.training.elevator.ifaces.IStories;
import by.training.elevator.ifaces.IStoriesRendering;

/**
 * Validating the process of transportation
 *
 */
public class TransportationValidator {

    /**
     * @param stories
     *            - floor system
     */
    public static void validate(final IStories stories, final IStoriesRendering rendering) {

        int passengersNumber = stories.getPassengersNumber();

        rendering.logOrPrint(
                getValidation(Constants.MESSAGE_SEND_PASSENGERS, (stories.getPassengersNumberDispatchStories() == 0)));
        rendering.logOrPrint(
                getValidation(Constants.MESSAGE_EMPTY_ELEVATOR, (stories.getPassengersNumberElevatorContainer() == 0)));
        rendering.logOrPrint(getValidation(Constants.MESSAGE_ARRIVED_PASSENGERS, (stories.isPassengersArrived())));
        rendering.logOrPrint(getValidation(Constants.MESSAGE_TRANSPORTED_PASSENGERS,
                (stories.getPassengersNumberArrivalStories() == passengersNumber)));
    }

    /**
     * Returns the full message validation
     *
     * @param message
     *            - message validation
     * @param validation
     *            - validation value
     * @return - message validation with the result of the validation
     */
    public static String getValidation(final String message, final boolean validation) {
        StringBuilder result = new StringBuilder(message);
        if (validation) {
            result.append(Constants.MESSAGE_SUCCESS);
        } else {
            result.append(Constants.MESSAGE_FAILURE);
        }
        return result.toString();
    }

}
