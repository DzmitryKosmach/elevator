package by.training.elevator;

import org.apache.logging.log4j.message.Message;

public final class Constants {

    public static final String FILE_PROPERTIES = "config";
    public static final String KEY_STORIES_NUMBER = "storiesNumber";
    public static final String KEY_ELEVATOR_CAPACITY = "elevatorCapacity";
    public static final String KEY_PASSENGERS_NUMBER = "passengersNumber";
    public static final String KEY_ANIMATION_BOOST = "animationBoost";
    public static final String KEY_ACTION = "action";
    public static final String KEY_BUILDING = "building";
    public static final String KEY_START = "START";
    public static final String KEY_ABORT = "ABORT";
    public static final String KEY_BOOST = "boost";

    public static final int BOOST = 1000;
    public static final int START_STORY = 1;

    public static final String URL_MAIN = "http://localhost:8080/elevator/main";
    public static final String URL_PARAM = "?%s=%d&%s=%d&%s=%d&%s=%d";
    public static final String JUMP_MAIN = "/default.jsp";
    public static final String ACTION_MAIN = "/start";

    public static final String MESSAGE_SUCCESS = "success!";
    public static final String MESSAGE_FAILURE = "failure!";
    public static final String MESSAGE_SEND_PASSENGERS = "Sent to all passengers: ";
    public static final String MESSAGE_EMPTY_ELEVATOR = "They went all the passengers out of the elevator: ";
    public static final String MESSAGE_ARRIVED_PASSENGERS = "All the passengers arrived at my story and they transportationState - COMPLETED: ";
    public static final String MESSAGE_TRANSPORTED_PASSENGERS = "All passengers transported: ";
    public static final String MESSAGE_STORY = "!!!!!!!!_Story_!!!!!!!!  -  ";
    public static final String MESSAGE_PASSANGER_ID = " (passengerId - %d)\n";

    public static final String ERROR_CANT_START_BROW = "Can't start browser!";
    public static final String ERROR_CANT_READ_PROPERTY = "Can't read property file!";
    public static final String MESSAGE_FROM_STORY = " (from story-%d to story-%d)";
    public static final String PASSENGER_ON_STORY = " (passangerID - %d on story-%d)";
    public static final String ERROR_PROCESS_COMPLETED = "The process has been completed.";
    public static final String ERROR_PROCESS_ABORTED = "The process has been aborted.";

}
