package by.training.elevator;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Generator unique id
 *
 */
public final class IDGenerator {
    private static AtomicInteger id = new AtomicInteger(0);

    private IDGenerator() {

    };

    /**     
     * @return following a unique id
     */
    public static int getNextID() {
        return id.incrementAndGet();
    }
}
