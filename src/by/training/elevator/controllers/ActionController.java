package by.training.elevator.controllers;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.training.elevator.Constants;
import by.training.elevator.ElevatorController;
import by.training.elevator.TransportationValidator;
import by.training.elevator.container.Building;

/**
 * Servlet implementation class ActionController
 */
@WebServlet(name = "ActionController", urlPatterns = "/action")
public class ActionController extends AbstractBaseController {
    private static final long serialVersionUID = 1L;

    @Override
    protected void performTask(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        final String ACTION = request.getParameter(Constants.KEY_ACTION).toUpperCase();
        ServletContext context = getServletConfig().getServletContext();
        Building building = (Building) context.getAttribute(Constants.KEY_BUILDING);
        ElevatorController elevatorController = building.getElevatorController();
        if (ACTION.equals(Constants.KEY_START)) {

            elevatorController.start();
            TransportationValidator.validate(building.getStories(), building.getElevatorController().getStoriesRendering());
        }

        if (ACTION.equals(Constants.KEY_ABORT)) {

            elevatorController.abort();
        }

    }
}
