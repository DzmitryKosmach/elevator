package by.training.elevator.controllers;

import java.util.ArrayList;

import by.training.elevator.beans.StoryRendering;

public class StoriesRendering extends ArrayList<StoryRendering> {

    private int curentStory;

    public StoriesRendering(int storiesNumber, int curentStory) {
        super();
        this.curentStory = curentStory;
        for (int i = 0; i < storiesNumber; i++) {
            add(new StoryRendering());
        }
    }

    public synchronized int getCurentStory() {
        return curentStory;
    }

    public synchronized void setCurentStory(final int curentStory) {
        this.curentStory = curentStory;
    }

}
