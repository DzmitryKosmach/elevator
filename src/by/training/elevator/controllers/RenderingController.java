package by.training.elevator.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ListIterator;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.training.elevator.Constants;
import by.training.elevator.ElevatorController;
import by.training.elevator.beans.StoryRendering;
import by.training.elevator.container.Building;
import by.training.elevator.container.StoriesRendering;

/**
 * Servlet implementation class RenderingController
 */
@WebServlet(name = "RenderingController", urlPatterns = "/rendering")
public class RenderingController extends AbstractBaseController {
    private static final long serialVersionUID = 1L;

    @Override
    protected void performTask(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletContext context = getServletConfig().getServletContext();
        Building building = (Building) context.getAttribute(Constants.KEY_BUILDING);
        ElevatorController elevatorController = building.getElevatorController();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        StoriesRendering rendering = (StoriesRendering) elevatorController.getStoriesRendering();

        int curentStory = rendering.size();
        ListIterator<StoryRendering> listIterator = rendering.listIterator(curentStory);
        while (listIterator.hasPrevious()) {
            StoryRendering story = listIterator.previous();

            out.println("<tr>");

            out.printf("<td>%s</td>", generateHTML(story.getDispatch()));

            if (rendering.getCurentStory() == curentStory) {
                out.printf("<td class='curentStory'>%s</td>", generateHTML(story.getElevator()));
            } else {
                out.printf("<td>%s</td>", generateHTML(story.getElevator()));
            }

            curentStory--;

            out.printf("<td>%s</td>", generateHTML(story.getArrival()));

            out.println("</tr>");
        }
    }

    private String generateHTML(int number) {
        if (number != 0) {
            return "<b>" + number + "</b><span></span>";
        } else {
            return "&nbsp;";
        }
    }

}
