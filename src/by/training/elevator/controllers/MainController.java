package by.training.elevator.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.training.elevator.Constants;
import by.training.elevator.ElevatorController;
import by.training.elevator.container.Building;
import by.training.elevator.container.DispatchStoryContainer;
import by.training.elevator.container.StoriesRendering;
import by.training.elevator.container.Story;
import by.training.elevator.factoties.StoriesRenderingFactories;

/**
 * Servlet implementation class MainController
 */
@WebServlet(name = "MainController", urlPatterns = "/main")
public class MainController extends AbstractBaseController {
    private static final long serialVersionUID = 1L;

    @Override
    protected void performTask(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            int storiesNumber = Integer.parseInt(request.getParameter(Constants.KEY_STORIES_NUMBER));
            int elevatorCapacity = Integer.parseInt(request.getParameter(Constants.KEY_ELEVATOR_CAPACITY));
            int passengersNumber = Integer.parseInt(request.getParameter(Constants.KEY_PASSENGERS_NUMBER));
            int animationBoost = Integer.parseInt(request.getParameter(Constants.KEY_ANIMATION_BOOST));

            Building building = new Building(storiesNumber, elevatorCapacity, passengersNumber, animationBoost);

            ServletContext context = getServletConfig().getServletContext();
            context.setAttribute(Constants.KEY_BUILDING, building);
            jumpPage(Constants.JUMP_MAIN, request, response);

        } catch (NumberFormatException e) {
            System.err.println("Can't read property!");
            System.err.println(e.getMessage());
        }

    }
}
