package by.training.elevator.factoties;

import by.training.elevator.container.StoriesRendering;
import by.training.elevator.container.StoriesRenderingConsole;
import by.training.elevator.ifaces.IStories;
import by.training.elevator.ifaces.IStoriesRendering;

/**
 * Rendering factory
 *
 */
public class StoriesRenderingFactories {

    public static IStoriesRendering getStoriesRendering(final IStories stories, final int animationBoost) {
        if (animationBoost > 0) {
            return new StoriesRendering(stories, animationBoost);
        } else {
            return new StoriesRenderingConsole();
        }
    }
}
