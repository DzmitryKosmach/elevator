package by.training.elevator;

import java.util.concurrent.Semaphore;

import by.training.elevator.beans.Passenger;

/**
 * The transportation task for the carriage of passengers
 *
 */
public class TransportationTask implements Runnable {

    private Passenger passenger;
    private IState currentState;
    private Semaphore boadSemaphore;
    private ElevatorController elevatorController;

    /**
     * The constructor with parameters
     *
     * @param passenger
     *            - passenger who need to carry
     * @param boadSemaphore
     *            - semaphore, giving permission for the entrance to the elevator
     * @param elevatorController
     *            - elevator operator
     */
    public TransportationTask(final Passenger passenger, final Semaphore boadSemaphore,
            final ElevatorController elevatorController) {
        super();
        this.passenger = passenger;
        passenger.setTask(this);
        this.boadSemaphore = boadSemaphore;
        this.elevatorController = elevatorController;
        currentState = new NotStartedState();
    }

    /**
     * @return semaphore, giving permission for the entrance to the elevator
     */
    public Semaphore getBoadSemaphore() {
        return boadSemaphore;
    }

    /**
     * @return associate passenger
     */
    public Passenger getPassenger() {
        return passenger;
    }

    /**
     * @return the current state of transportation
     */
    public IState getCurrentState() {
        return currentState;
    }

    /**
     * Changes the state of transportation
     */
    public void processing() {
        currentState.processing();
    }

    /**
     * Sets transportation state to the abort
     */
    public void abort() {
        currentState.toAbort();
    }

    @Override
    public void run() {

        try {
            processing();

            boadSemaphore.acquire();

            elevatorController.boadPassenger(passenger);
            elevatorController.getStoryDirectionLatch().countDown();            
            Semaphore deabodingSemaphore = elevatorController.getDeabodingSemaphore();
            do {
                deabodingSemaphore.acquire();                
                if (passenger.getDestinationStory() == elevatorController.getNumberCurrentStory()) {
                    elevatorController.deboadPassenger(passenger);
                    elevatorController.getStoryDeboadingLatch().countDown();                    
                    break;
                }
                elevatorController.getElevatorDeboadingLatch().await();
            } while (true);

            processing();

        } catch (InterruptedException e) {
            abort();
            processing();
            elevatorController.getStoriesRendering()
                    .logOrError(String.format(
                            TransportationActions.ABORTING_TRANSPORTATION + Constants.MESSAGE_PASSANGER_ID,
                            passenger.getPassengerID()));
        }
    }

    private interface IState {
        void processing();

        void toAbort();
    }    

    private class NotStartedState implements IState {        

        @Override
        public void processing() {
            currentState = new InProgressState();
            passenger.setState(TransportationState.IN_PROGRESS);
        }

        @Override
        public void toAbort() {
            currentState = new AbortedState();
        }
    }

    private class InProgressState implements IState {        

        @Override
        public void processing() {
            currentState = new CompletedState();
            passenger.setState(TransportationState.COMPLETED);
        }

        @Override
        public void toAbort() {
            currentState = new AbortedState();
        }
    }

    private class CompletedState implements IState {
        
        @Override
        public void processing() {
            
        }

        @Override
        public void toAbort() {
            throw new UnsupportedOperationException(Constants.ERROR_PROCESS_COMPLETED);
        }
    }

    private class AbortedState implements IState {

        @Override
        public void processing() {
            passenger.setState(TransportationState.ABORTED);
            System.out.println(passenger);
        }

        @Override
        public void toAbort() {
            throw new UnsupportedOperationException(Constants.ERROR_PROCESS_ABORTED);
        }

    }

}
