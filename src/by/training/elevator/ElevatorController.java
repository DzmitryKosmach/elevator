package by.training.elevator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import by.training.elevator.beans.Passenger;
import by.training.elevator.container.Story;
import by.training.elevator.ifaces.IStories;
import by.training.elevator.ifaces.IStoriesRendering;

/**
 * The elevator operator
 *
 */
public class ElevatorController {

    private ExecutorService executorService;
    private CountDownLatch storyDeboadingLatch;
    private CountDownLatch storyDirectionLatch;
    private CountDownLatch elevatorDeboadingLatch;
    private ReentrantLock locker;
    private IStories stories;
    private AtomicInteger permits;
    private Semaphore deabodingSemaphore;
    private IStoriesRendering rendering;
    private Set<TransportationTask> tasks;

    /**
     * The constructor without parameters
     */
    public ElevatorController() {
        super();
    }

    /**
     * The constructor with parameters
     *
     * @param stories
     *            - floor system
     * @param elevatorCapacity
     *            - capacity elevator
     * @param rendering
     *            - rendering system
     */
    public ElevatorController(final IStories stories, final int elevatorCapacity, final IStoriesRendering rendering) {
        this.stories = stories;
        this.rendering = rendering;
        permits = new AtomicInteger(elevatorCapacity);
        deabodingSemaphore = new Semaphore(0, true);
        locker = new ReentrantLock();
        tasks = new HashSet<>();
        executorService = Executors.newFixedThreadPool(stories.getPassengersNumber());
        initTransportationTasks();
        startTransportationTasks();
    }

    /**
     * Sets IStories stories
     *
     * @param stories
     *            - floor system
     */
    public void setStories(final IStories stories) {
        this.stories = stories;
    }

    /**
     * @return - latch, responsible for the entrance to the elevator
     */
    public CountDownLatch getElevatorDeboadingLatch() {
        return elevatorDeboadingLatch;
    }

    /**
     * @return - latch, is responsible for the way out of the elevator
     */
    public CountDownLatch getStoryDeboadingLatch() {
        return storyDeboadingLatch;
    }

    /**
     * @return - latch, responsible for moving between floors elevator
     */
    public CountDownLatch getStoryDirectionLatch() {
        return storyDirectionLatch;
    }

    /**
     * @return - semaphore gives permission for the way out of the elevator
     */
    public Semaphore getDeabodingSemaphore() {
        return deabodingSemaphore;
    }

    /**
     * Starts movement of the elevator and the elevator moves in a cycle up and
     * down until in the dispatcher containers are the passengers
     */
    public void start() {

        try {
            rendering.logOrPrint(TransportationActions.STARTING_TRANSPORTATION.toString());
            while (stories.getPassengersNumberDispatchStories() > 0) {
                do {
                    rendering.logOrPrint(Constants.MESSAGE_STORY + stories.getNumberCurentStory());
                    permitDeboadPassengersStory();
                    permitBoadPassengersStory();
                    rendering.logOrPrint(
                            String.format(TransportationActions.MOVING_ELEVATOR + Constants.MESSAGE_FROM_STORY,
                                    stories.getNumberCurentStory(), stories.getNextStory()));

                } while (stories.nextStory());
                rendering.updateRendering();
            }
            permitDeboadPassengersStory();
            rendering.logOrPrint(TransportationActions.COMPLETION_TRANSPORTATION.toString());

        } catch (InterruptedException e) {
            rendering.logOrError(TransportationActions.ABORTING_TRANSPORTATION.toString());
        }
    }

    /**
     * It gives permission for passengers on the floor for the entrance to the
     * elevator, in the amount agreed with the number of seats of the elevator
     * and the number of passengers who wish to move in the direction of
     * movement of the elevator and wait until all passengers are allowed to
     * pass elevator.
     *
     * @throws InterruptedException
     */
    private void permitBoadPassengersStory() throws InterruptedException {
        int numberPassengersDirection = stories.getCurentDispatchStory()
                .getNumberPassengersDirection(stories.getCurrentSemaphore());
        if (numberPassengersDirection > permits.get()) {
            numberPassengersDirection = permits.get();
        }
        storyDirectionLatch = new CountDownLatch(numberPassengersDirection);
        Semaphore currentSemaphore = stories.getCurrentSemaphore();
        currentSemaphore.release(numberPassengersDirection);
        storyDirectionLatch.await();

        rendering.updateRendering();
    }

    /**
     * Moves the passenger in the passenger elevator.
     *
     * @param passenger
     *            - the passenger to be transplanted
     */
    public void boadPassenger(final Passenger passenger) {
        locker.lock();
        try {
            stories.boadPassengerElevator(passenger);
            permits.decrementAndGet();
            rendering.updateRendering();
            rendering.logOrPrint(
                    String.format(TransportationActions.BOARDING_OF_PASSENGER + Constants.PASSENGER_ON_STORY,
                            passenger.getPassengerID(), stories.getNumberCurentStory()));

        } finally {
            locker.unlock();
        }
    }

    /**
     * It gives permission to passengers in the elevator on the way out, in the
     * number of those wishing to enter the current floor and waits for all
     * passengers wishing to come.
     *
     * @throws InterruptedException
     */
    private void permitDeboadPassengersStory() throws InterruptedException {

        int numberPassengersDeboading = stories.getPassengersDeboading();
        if (numberPassengersDeboading != 0) {
            storyDeboadingLatch = new CountDownLatch(numberPassengersDeboading);
            elevatorDeboadingLatch = new CountDownLatch(1);
            deabodingSemaphore.release(stories.getElevatorContainer().getSize());
            storyDeboadingLatch.await();
            elevatorDeboadingLatch.countDown();
        }
        rendering.updateRendering();
    }

    /**
     * Moves on the floor of the passenger
     *
     * @param passenger
     *            - passenger who will be moved
     */
    public void deboadPassenger(final Passenger passenger) {
        locker.lock();
        try {
            stories.deboadPassengerElevator(passenger);
            permits.incrementAndGet();
            rendering.updateRendering();
            rendering.logOrPrint(
                    String.format(TransportationActions.DEBOARDING_OF_PASSENGER + Constants.PASSENGER_ON_STORY,
                            passenger.getPassengerID(), stories.getNumberCurentStory()));

        } finally {
            locker.unlock();
        }
    }

    /**
     * @return the current number of the floor on which the elevator
     */
    public int getNumberCurrentStory() {
        return stories.getNumberCurentStory();
    }    

    /**
     * Immediately interrupts the transport process
     */
    public void abort() {
        executorService.shutdownNow();
    }

    /**
     * Initializes each passenger in the dispatch of containers
     * TransportationTasks
     */
    public void initTransportationTasks() {

        for (int i = 1; i <= stories.getStoriesNumber(); i++) {
            Story currentStory = stories.getStory(i);
            Iterator<Passenger> passengers = currentStory.getDispatchStoryContainer().getIterator();
            while(passengers.hasNext()){
                Semaphore semaphore;
                Passenger passenger = passengers.next();
                if (passenger.getDestinationStory() > i) {
                    semaphore = currentStory.getSemaphoreUp();
                } else {
                    semaphore = currentStory.getSemaphoreDown();
                }
                tasks.add(new TransportationTask(passenger, semaphore, this));
            }
        }
        rendering.updateRendering();
    }

    /**
     * It activates all TransportationTasks
     */
    public void startTransportationTasks() {
        Iterator<TransportationTask> iterator = tasks.iterator();
        while (iterator.hasNext()) {
            executorService.execute(iterator.next());
        }
    }

    /**   
     * @return Set TransportationTasks
     */
    public Set<TransportationTask> getTasks() {
        return tasks;
    }

    /**     
     * @return rendering system
     */
    public IStoriesRendering getStoriesRendering() {
        return rendering;
    }

}
