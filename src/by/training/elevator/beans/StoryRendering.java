package by.training.elevator.beans;

/**
 * It describes the state the floor for rendering
 *
 */
public class StoryRendering {
    private int dispatch;
    private int elevator;
    private int arrival;

    /**
     * The constructor without parameters
     */
    public StoryRendering() {
        super();
    }

    /**
     * The constructor with parameters
     *
     * @param dispatch
     *            - the number of passengers in the departure container
     * @param elevator
     *            - the number of passengers in the elevator
     * @param arrival -
     *            the number of passengers arriving in container
     */
    public StoryRendering(final int dispatch, final int elevator, final int arrival) {
        super();
        this.dispatch = dispatch;
        this.elevator = elevator;
        this.arrival = arrival;
    }

    /**
     * @return the number of passengers in the despatch container
     */
    public int getDispatch() {
        return dispatch;
    }

    /**
     * @return the number of passengers in the elevator
     */
    public int getElevator() {
        return elevator;
    }

    /**
     * @return the number of passengers arriving in container
     */
    public int getArrival() {
        return arrival;
    }

    /**
     * Sets number of passengers in the dispatch container
     *
     * @param dispatch - the number of passengers in the dispatch container
     */
    public void setDispatch(final int dispatch) {
        this.dispatch = dispatch;
    }

    /**
     * Sets number of passengers in the elevator
     *
     * @param elevator - the number of passengers in the elevator
     */
    public void setElevator(final int elevator) {
        this.elevator = elevator;
    }

    /**
     * Sets number of passengers in the arrival of the container
     *
     * @param arrival - the number of passengers arriving in container
     */
    public void setArrival(final int arrival) {
        this.arrival = arrival;
    }

}
