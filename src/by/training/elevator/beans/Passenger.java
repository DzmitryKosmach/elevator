package by.training.elevator.beans;

import by.training.elevator.IDGenerator;
import by.training.elevator.TransportationState;
import by.training.elevator.TransportationTask;

/**
 * Describes passenger
 *
 */
public class Passenger {
    private int passengerID;
    private int destinationStory;
    private TransportationState state;
    private TransportationTask task;

    /**
     * The constructor without parameters
     */
    public Passenger() {
        super();
    }

    /**
     * The constructor with a parameter
     *
     * @param destinationStory - destination floor
     */
    public Passenger(final int destinationStory) {
        super();
        this.passengerID = IDGenerator.getNextID();
        this.destinationStory = destinationStory;
        state = TransportationState.NOT_STARTED;
    }

    /**
     * @return passengerID - id passenger
     */
    public int getPassengerID() {
        return passengerID;
    }

    /**
     * @return destinationStory - destination floor
     */
    public int getDestinationStory() {
        return destinationStory;
    }

    /**
     * @return transportation state
     */
    public TransportationState getState() {
        return state;
    }

    /**
     * Sets transportation state
     *
     * @param state - transportation state
     */
    public void setState(final TransportationState state) {
        this.state = state;
    }

    /**
     * @return associated transportationTask
     */
    public TransportationTask getTask() {
        return task;
    }

    /**
     * Sets transportationTask
     *
     * @param task - transportationTask
     */
    public void setTask(final TransportationTask task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "Passenger [passengerID=" + passengerID + ", destinationStory=" + destinationStory + ", state=" + state
                + "]";
    }

}
