package by.training.elevator;

import java.net.URI;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import by.training.elevator.container.Building;
import by.training.elevator.container.StoriesRendering;
import by.training.elevator.container.StoriesRenderingConsole;
import by.training.elevator.ifaces.IStoriesRendering;

/**
 * It is the entry point into the application
 *
 */
public class Main {

    /**
     * Called when the application starts
     *
     * @param args
     *            - application settings (not used)
     */
    public static void main(final String[] args) {

        IStoriesRendering rendering;
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(Constants.FILE_PROPERTIES, Locale.getDefault());
            int storiesNumber = parseInt(bundle.getString(Constants.KEY_STORIES_NUMBER));
            int elevatorCapacity = parseInt(bundle.getString(Constants.KEY_ELEVATOR_CAPACITY));
            int passengersNumber = parseInt(bundle.getString(Constants.KEY_PASSENGERS_NUMBER));
            int animationBoost = parseInt(bundle.getString(Constants.KEY_ANIMATION_BOOST));

            if (animationBoost == 0) {
                Building building = new Building(storiesNumber, elevatorCapacity, passengersNumber, animationBoost);
                building.getElevatorController().start();
                TransportationValidator.validate(building.getStories(),
                        building.getElevatorController().getStoriesRendering());
            } else {
                try {
                    String uriString = String.format(Constants.URL_MAIN + Constants.URL_PARAM,
                            Constants.KEY_STORIES_NUMBER, storiesNumber, Constants.KEY_ELEVATOR_CAPACITY,
                            elevatorCapacity, Constants.KEY_PASSENGERS_NUMBER, passengersNumber,
                            Constants.KEY_ANIMATION_BOOST, animationBoost);
                    URI uri = new URI(uriString);
                    java.awt.Desktop.getDesktop().browse(uri);
                } catch (Exception e) {
                    rendering = new StoriesRendering();
                    rendering.logOrError(Constants.ERROR_CANT_START_BROW);
                    rendering.logOrError(e.getMessage());
                }
            }
        } catch (MissingResourceException | NumberFormatException e) {
            rendering = new StoriesRenderingConsole();
            rendering.logOrError(Constants.ERROR_CANT_READ_PROPERTY);
            rendering.logOrError(e.getMessage());
        }
    }

    private static int parseInt(final String str) throws NumberFormatException {
        return Integer.parseInt(str);
    }
}
