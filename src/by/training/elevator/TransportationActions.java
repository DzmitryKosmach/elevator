package by.training.elevator;

/**
 * List of transportation actions
 *
 */
public enum TransportationActions {
    STARTING_TRANSPORTATION, COMPLETION_TRANSPORTATION, ABORTING_TRANSPORTATION, MOVING_ELEVATOR, BOARDING_OF_PASSENGER, DEBOARDING_OF_PASSENGER;
}
