package by.training.elevator;


/**
 * List of transportation states
 *
 */
public enum TransportationState {
    NOT_STARTED, IN_PROGRESS, COMPLETED, ABORTED
}
