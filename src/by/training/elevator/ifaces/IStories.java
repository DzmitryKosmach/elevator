package by.training.elevator.ifaces;

import java.util.List;
import java.util.concurrent.Semaphore;

import by.training.elevator.beans.Passenger;
import by.training.elevator.container.ArrivalStoryContainer;
import by.training.elevator.container.DispatchStoryContainer;
import by.training.elevator.container.ElevatorContainer;
import by.training.elevator.container.Story;

/**
 * Describes stories behavior of the system
 *
 */
public interface IStories {
    
    /**     
     * @return the total number of passengers
     */
    int getPassengersNumber();

    /**     
     * @return the direction of movement upwards
     */
    boolean getDirectionUp();

    /**
     * Returns the current semaphore in accordance with the direction of
     * movement of the elevator
     *
     * @return current semaphore
     */
    Semaphore getCurrentSemaphore();

    /**     
     * @return number of stories
     */
    int getStoriesNumber();

    /**     
     * @return list of stories
     */
    List<Story> getStories();

    /**     
     * @param numberStory
     *            - number story
     * @return story on floor number
     */
    Story getStory(int numberStory);

    /**
     * Moves the elevator to the next floor
     *
     * @return true if the movement up and down is not finished, otherwise false
     */
    boolean nextStory();

    /**     
     * @return number the next floor
     */
    int getNextStory();

    /**
     * @return current Story
     */
    Story getCurrentStory();

    /**     
     * @return the current dispatch container
     */
    DispatchStoryContainer getCurentDispatchStory();

    /**
     * Returns dispatch container corresponding to the number of floor
     *
     * @param story
     *            - floor number
     * @return dispatch container corresponding to the number of floor
     */
    DispatchStoryContainer getDispatchStory(int story);

    /**    
     * @return current arrival container
     */
    ArrivalStoryContainer getCurentArrivalStory();

    /**     
     * @return elevator container
     */
    ElevatorContainer getElevatorContainer();

    /**
     * Move the passenger from dispatch container to the elevator
     *
     * @param passenger
     *            - the passenger is, you want to move
     */
    void boadPassengerElevator(Passenger passenger);

    /**
     * Move the passenger from elevator to the arrival container 
     *
     * @param passenger
     *            - the passenger is, you want to move
     */
    void deboadPassengerElevator(Passenger passenger);

    /**    
     * @return the number of passengers in the dispatch containers
     */
    int getPassengersNumberDispatchStories();

    /**     
     * @return the number of passengers in the arrival containers
     */
    int getPassengersNumberArrivalStories();

    /**
     * Checks whether all the passengers arrived at their floor
     *
     * @return true, if all the passengers arrived at their floor, false otherwise
     */
    boolean isPassengersArrived();

    /**    
     * @return the number of passengers in the elevator
     */
    int getPassengersNumberElevatorContainer();

    /**     
     * @return number of the current floor
     */
    int getNumberCurentStory();

    /**     
     * @return the number of passengers output at the current floor
     */
    int getPassengersDeboading();

    /**
     * Places random passengers in the dispatch containers
     */
    void setPassengers();
}
