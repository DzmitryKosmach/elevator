package by.training.elevator.ifaces;

/**
 * Rendering - the current status of the transportation process for the visual part of the application
 *
 */
public interface IStoriesRendering {

    /**
     * It updates the current rendering state
     */
    void updateRendering();

    /**
     * Does rendering delay
     */
    void sleep();
    
    /**
     * Displays message through the logging level INFO or in system.out.println
     */
    void logOrPrint(String message);
    
    /**
     * Displays message through the logging level ERROR or in system.err.println
     */
    void logOrError(String message);
    
}
