package by.training.elevator.ifaces;

import by.training.elevator.ElevatorController;
import by.training.elevator.container.Stories;

public interface IBuilding {
    
    /**    
     * @return floor system
     */
    Stories getStories();
    
    /**     
     * @return elevator operator
     */
    ElevatorController getElevatorController();
}
