package by.training.elevator.container;

import java.util.concurrent.Semaphore;

/**
 * Floor
 *
 */
public class Story {
    private int numberStory;
    private DispatchStoryContainer dispatchStoryContainer;
    private ArrivalStoryContainer arrivalStoryContainer;
    private final Semaphore SEMAPHORE_UP;
    private final Semaphore SEMAPHORE_DOWN;

    /**
     * The constructor with parameters
     * 
     * @param numberStory
     *            - number floor
     * @param dispatchStoryContainer
     *            - dispatch container
     * @param arrivalStoryContainer
     *            - arrival container
     */
    public Story(final int numberStory, final DispatchStoryContainer dispatchStoryContainer,
            final ArrivalStoryContainer arrivalStoryContainer) {
        super();
        this.numberStory = numberStory;
        this.dispatchStoryContainer = dispatchStoryContainer;
        this.arrivalStoryContainer = arrivalStoryContainer;
        SEMAPHORE_UP = new Semaphore(0, true);
        SEMAPHORE_DOWN = new Semaphore(0, true);
    }

    public int getNumberStory() {
        return numberStory;
    }

    /**
     * @return dispatch container
     */
    public DispatchStoryContainer getDispatchStoryContainer() {
        return dispatchStoryContainer;
    }

    /**
     * @return arrival container
     */
    public ArrivalStoryContainer getArrivalStoryContainer() {
        return arrivalStoryContainer;
    }

    /**
     * @return the number of passengers in the dispatch container
     */
    public int getNumberPassengersDispatchStory() {
        return dispatchStoryContainer.getSize();
    }

    /**    
     * @return the number of passengers in the arrival container
     */
    public int getNumberPassengersArrivalStory() {
        return arrivalStoryContainer.getSize();
    }

    /**    
     * @return responsibility for the input semaphore when moving up
     */
    public Semaphore getSemaphoreUp() {
        return SEMAPHORE_UP;
    }

    /**    
     * @return responsibility for the input semaphore when moving down
     */
    public Semaphore getSemaphoreDown() {
        return SEMAPHORE_DOWN;
    }

}
