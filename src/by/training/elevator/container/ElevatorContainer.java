package by.training.elevator.container;

/**
 * Elevator container
 *
 */
public class ElevatorContainer extends Container {
    private int elevatorCapacity;

    /**
     The constructor with a parameter
     *
     * @param elevatorCapacity - elevator capacity
     */
    public ElevatorContainer(final int elevatorCapacity) {
        super();
        this.elevatorCapacity = elevatorCapacity;
    }

    /**
     * @return elevator capacity
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * Checks whether the elevator is full
     *
     * @return false if the elevator is not full, otherwise true
     */
    public boolean isFull() {
        if (getSize() < elevatorCapacity) {
            return false;
        }
        return true;
    }

}
