package by.training.elevator.container;

import java.util.ArrayList;

import by.training.elevator.Constants;
import by.training.elevator.beans.StoryRendering;
import by.training.elevator.ifaces.IStories;
import by.training.elevator.ifaces.IStoriesRendering;

public class StoriesRendering extends ArrayList<StoryRendering> implements IStoriesRendering {

    private IStories stories;
    private int curentStory;
    private int storiesNumber;
    private int animationBoost;

    /**
     * The constructor without parameters
     */
    public StoriesRendering() {

    }

    /**
     * The constructor with parameters
     *
     * @param stories
     *            - floor system
     * @param animationBoost
     *            - rendering boost
     */
    public StoriesRendering(final IStories stories, int animationBoost) {
        super();
        this.stories = stories;
        animationBoost = Math.abs(animationBoost);
        if (animationBoost > Constants.BOOST) {
            animationBoost = Constants.BOOST;
        }
        this.animationBoost = Constants.BOOST / animationBoost;
        curentStory = stories.getNumberCurentStory();
        storiesNumber = stories.getStoriesNumber();
        for (int i = 0; i < storiesNumber; i++) {
            add(new StoryRendering());
        }
    }

    /**
     * @return number of the current floor
     */
    public synchronized int getCurentStory() {
        return curentStory;
    }

    /**
     * Sets number of the current floor
     *
     * @param curentStory
     *            - number of the current floor
     */
    public void setCurentStory(final int curentStory) {
        this.curentStory = curentStory;
    }

    @Override
    public void updateRendering() {
        curentStory = stories.getNumberCurentStory();
        for (int i = 0; i < storiesNumber; i++) {
            StoryRendering storyRendering = get(i);
            Story story = stories.getStories().get(i);
            storyRendering.setDispatch(story.getNumberPassengersDispatchStory());
            if (i == (curentStory - 1)) {
                storyRendering.setElevator(stories.getElevatorContainer().getSize());
            } else {
                storyRendering.setElevator(0);
            }
            storyRendering.setArrival(story.getNumberPassengersArrivalStory());
        }
        setCurentStory(curentStory);
        sleep();
    }

    @Override
    public void sleep() {
        try {
            Thread.sleep(animationBoost);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logOrPrint(final String message) {
        System.out.println(message);
    }

    @Override
    public void logOrError(final String message) {
        System.err.println(message);
    }

}
