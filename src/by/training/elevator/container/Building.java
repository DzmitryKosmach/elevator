package by.training.elevator.container;

import by.training.elevator.ElevatorController;
import by.training.elevator.factoties.StoriesRenderingFactories;
import by.training.elevator.ifaces.IBuilding;

/**
 * The building with the system of floors and elevator operator
 *
 */
public class Building implements IBuilding {
    private Stories stories;
    private ElevatorController elevatorController;

    /**
     * The constructor with parameters
     *
     * @param storiesNumber
     *            - number of floors in the building
     * @param elevatorCapacity
     *            - elevator capacity
     * @param passengersNumber
     *            - number of passengers
     * @param animationBoost
     *            - rendering boost
     */
    public Building(final int storiesNumber, final int elevatorCapacity, final int passengersNumber,
            final int animationBoost) {
        super();
        stories = new Stories(storiesNumber, passengersNumber, elevatorCapacity);
        elevatorController = new ElevatorController(stories, elevatorCapacity,
                StoriesRenderingFactories.getStoriesRendering(stories, animationBoost));
    }

    @Override
    public Stories getStories() {
        return stories;
    }

    @Override
    public ElevatorController getElevatorController() {
        return elevatorController;
    }

}
