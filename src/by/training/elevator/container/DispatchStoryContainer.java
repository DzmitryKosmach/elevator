package by.training.elevator.container;

import java.util.Iterator;
import java.util.concurrent.Semaphore;
import by.training.elevator.beans.Passenger;

/**
 * Dispatch container
 *
 */
public class DispatchStoryContainer extends Container {

    /**
     * Returns the number of passengers in the despatch container in the
     * direction of movement of the elevator
     *
     * @param semaphore
     *            - semaphore in the direction of movement of the elevator
     * @return the number of passengers in the dispatch container in the
     *         direction of movement of the elevator
     */
    public int getNumberPassengersDirection(final Semaphore semaphore) {
        int result = 0;
        Iterator<Passenger> iterator = getIterator();
        while (iterator.hasNext()) {
            if (iterator.next().getTask().getBoadSemaphore() == semaphore) {
                result++;
            }
        }
        return result;
    }
}
