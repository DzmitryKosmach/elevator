package by.training.elevator.container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.training.elevator.ifaces.IStoriesRendering;


/**
 *  Rendering for console 
 *
 */
public class StoriesRenderingConsole implements IStoriesRendering {

    private static final Logger LOG = LogManager.getLogger(StoriesRenderingConsole.class);
    
    @Override
    public void updateRendering() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public void logOrPrint(String message) {
        LOG.info(message);
    }

    @Override
    public void logOrError(String message) {
        LOG.error(message);        
    }     

}
