package by.training.elevator.container;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.concurrent.Semaphore;

import by.training.elevator.Constants;
import by.training.elevator.TransportationState;
import by.training.elevator.beans.Passenger;
import by.training.elevator.ifaces.IStories;

/**
 * implementation of IStories
 *
 */
public class Stories implements IStories {
    private Story currentStory;
    private int storiesNumber;
    private List<Story> stories;
    private ElevatorContainer elevatorContainer;
    private int passengersNumber;
    private boolean directionUp = true;
    private final int delta;
    private ListIterator<Story> storiesIterator;

    /**
     * The constructor with parameters
     *
     * @param storiesNumber
     *            - number of stories
     * @param passengersNumber
     *            - number of passengers
     * @param elevatorCapacity
     *            - elevator capacity
     */
    public Stories(final int storiesNumber, final int passengersNumber, final int elevatorCapacity) {
        super();
        this.storiesNumber = storiesNumber;
        this.passengersNumber = passengersNumber;
        elevatorContainer = new ElevatorContainer(elevatorCapacity);
        stories = new LinkedList<>();
        delta = 1;
        for (int i = 0; i < storiesNumber; i++) {
            stories.add(new Story(i + delta, new DispatchStoryContainer(), new ArrivalStoryContainer()));
        }
        setPassengers();
        storiesIterator = stories.listIterator(Constants.START_STORY - delta);
        currentStory = storiesIterator.next();
    }

    @Override
    public boolean getDirectionUp() {
        return directionUp;
    }

    @Override
    public int getPassengersNumber() {
        return passengersNumber;
    }

    @Override
    public Semaphore getCurrentSemaphore() {
        Story story = getCurrentStory();
        if (directionUp) {
            return story.getSemaphoreUp();
        } else {
            return story.getSemaphoreDown();
        }
    }

    @Override
    public int getStoriesNumber() {
        return storiesNumber;
    }

    @Override
    public List<Story> getStories() {
        return stories;
    }

    @Override
    public Story getStory(final int numberStory) {
        return stories.get(numberStory - delta);
    }

    @Override
    public boolean nextStory() {
        if (directionUp) {
            if (storiesIterator.hasNext()) {
                currentStory = storiesIterator.next();
                if (!storiesIterator.hasNext()) {
                    storiesIterator.previous();
                    directionUp = false;
                }
            }
        } else {
            if (storiesIterator.hasPrevious()) {
                currentStory = storiesIterator.previous();
                if (!storiesIterator.hasPrevious()) {
                    directionUp = true;
                    storiesIterator.next();
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int getNextStory() {
        if (directionUp) {
            return storiesIterator.nextIndex() + delta;
        }
        return storiesIterator.previousIndex() + delta;
    }

    @Override
    public Story getCurrentStory() {
        return currentStory;
    }

    @Override
    public DispatchStoryContainer getCurentDispatchStory() {
        return currentStory.getDispatchStoryContainer();
    }

    @Override
    public DispatchStoryContainer getDispatchStory(final int story) {
        return stories.get(story - delta).getDispatchStoryContainer();
    }

    @Override
    public ArrivalStoryContainer getCurentArrivalStory() {
        return currentStory.getArrivalStoryContainer();
    }

    @Override
    public ElevatorContainer getElevatorContainer() {
        return elevatorContainer;
    }

    @Override
    public void boadPassengerElevator(final Passenger passenger) {

        if (!elevatorContainer.isFull()) {
            DispatchStoryContainer dispatchStory = getCurentDispatchStory();
            if (dispatchStory.containsPassenger(passenger)) {
                dispatchStory.removePassenger(passenger);
                elevatorContainer.addPassenger(passenger);
            }
        }
    }

    @Override
    public void deboadPassengerElevator(final Passenger passenger) {

        if (elevatorContainer.containsPassenger(passenger)) {
            if (elevatorContainer.removePassenger(passenger)) {
                getCurentArrivalStory().addPassenger(passenger);
            }
        }
    }

    @Override
    public int getPassengersNumberDispatchStories() {
        int result = 0;
        for (Story story : stories) {
            result += story.getNumberPassengersDispatchStory();
        }
        return result;
    }

    @Override
    public int getPassengersNumberArrivalStories() {
        int result = 0;
        for (Story story : stories) {
            result += story.getNumberPassengersArrivalStory();
        }
        return result;
    }

    @Override
    public boolean isPassengersArrived() {
        for (int i = 1; i <= storiesNumber; i++) {

            Iterator<Passenger> iterator = stories.get(i - delta).getArrivalStoryContainer().getIterator();
            while (iterator.hasNext()) {
                Passenger passenger = iterator.next();
                if (passenger.getDestinationStory() != i
                        || !passenger.getState().equals(TransportationState.COMPLETED)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public int getPassengersNumberElevatorContainer() {
        return elevatorContainer.getSize();
    }

    @Override
    public int getNumberCurentStory() {
        return currentStory.getNumberStory();
    }

    @Override
    public int getPassengersDeboading() {
        int result = 0;
        Iterator<Passenger> iterator = elevatorContainer.getIterator();
        while (iterator.hasNext()) {
            if (iterator.next().getDestinationStory() == currentStory.getNumberStory()) {
                result++;
            }
        }
        return result;
    }

    @Override
    public void setPassengers() {
        Random random = new Random();
        for (int i = 1; i <= passengersNumber; i++) {
            int startingStory = random.nextInt(storiesNumber) + delta;
            int destinationStory;
            do {
                destinationStory = random.nextInt(storiesNumber) + delta;
            } while (startingStory == destinationStory);

            getDispatchStory(startingStory).addPassenger(new Passenger(destinationStory));
        }
    }
}
