package by.training.elevator.container;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import by.training.elevator.beans.Passenger;

/**
 * Abstract container class passengers
 *
 */
public abstract class Container {
    private Set<Passenger> passengers;

    /**
     * The constructor without parameters
     */
    public Container() {
        super();
        this.passengers = new HashSet<>();
    }

    /**
     * Adds passenger
     *
     * @param passenger
     *            - passenger who need to add
     */
    public void addPassenger(final Passenger passenger) {
        passengers.add(passenger);
    }

    /**
     * Removes passenger
     *
     * @param passenger
     *            - passenger who need to remove
     * @return true, if the passenger is removed, false otherwise
     */
    public boolean removePassenger(final Passenger passenger) {
        return passengers.remove(passenger);
    }

    /**
     * @return passengers iterator
     */
    public Iterator<Passenger> getIterator() {
        return passengers.iterator();
    }

    /**
     * @return number of passengers
     */
    public int getSize() {
        return passengers.size();
    }

    /**
     * Checks whether the passenger has
     *
     * @param passenger
     *            - passenger, the contents of which must be checked in the
     *            container
     * @return true, if the passenger is found, false otherwise
     */
    public boolean containsPassenger(final Passenger passenger) {
        return passengers.contains(passenger);
    }
}
