﻿$(document)
		.ready(
				function(e) {
					
					var attrStart = "START";
					var attrAbort = "ABORT";
					var attrViewLofFile = "VIEW_LOF_FILE";
					var nameViewLofFile = "VIEW LOG FILE";
					var attrName = "value";
					var completed = "COMPLETED!";
					
					rendering();
					
					$("#text_mess").val("");
					
					
					$("#button").click(function(e) {

						buttonThis = $(this);						
						action = buttonThis.attr(attrName);
						if (action == attrStart) {
							
							changeButton(buttonThis, attrName, attrAbort, attrAbort);
							$("#text_mess").val(attrStart + "\n");

							$.ajax({
								url : 'action' + '?action=' + action,
								success : function() {
									changeButton(buttonThis, attrName, attrViewLofFile, nameViewLofFile);
									append_text_mess(completed);
								}
							});
							
							interval_rendering = setInterval(() => {
								rendering();
							}, 100);	
						}
						
						if (action == attrAbort) {

							changeButton(buttonThis, attrName, attrViewLofFile, nameViewLofFile);							
							append_text_mess(attrAbort);							
							$.ajax({
								url : 'action' + '?action=' + action
							});							
							clearInterval(interval_rendering);					
						}
						
						if (action == attrViewLofFile){							
							
							$("#logFile").submit();
							
						}

						// $(this).attr("value","ABORT");

					});	
					
					
					function changeButton(button, attrName, attrValue, nameButton){						
						button.attr(attrName, attrValue);
						button.html(nameButton);
					}
					
					function append_text_mess(msg){
						$("#text_mess").val($("#text_mess").val() + msg +"\n");						
					};

										
					function rendering() {						
						$.ajax({
							url : 'rendering',
							success : function(data) {
								$("#elevator").html(data);
							}
						});
					}
					;
					
					
				});