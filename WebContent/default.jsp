<%@ page import="by.training.elevator.Constants"%>
<%@ taglib uri="/jstl/core" prefix="c"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title>Elevator</title>
<link rel="stylesheet" type="text/css" href="styles/style.css">
<link rel="stylesheet" type="text/css" href="fonts/f.css">

<script type="text/javascript"
	src="js/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="js/main.js"></script>


</head>
<body>
	<div id="wrapper">
<!-- 		<header id="header"></header> -->

		<main id="main">

		<div id="action">

			<button id="button" value="START">START</button>

		</div>

		<div id="building">
			<table>
				<tbody id="elevator">

				</tbody>
			</table>
		</div>		
		

		<div id="messages">
			<textarea id="text_mess"></textarea>
		</div>

		</main>

		<footer id="footer">
			<p>
				&copy; Developed by Dzmitry Kosmach <a
					href="mailto:dzmitry.kosmach@gmail.com">dzmitry.kosmach@gmail.com</a>
			</p>
		</footer>
	</div>
	
	<!------------end #wrapper----------------->
	<form id="logFile" action="http://localhost:8080/elevator/elevator.log">
		<input type="submit"> 
	</form>
	
</body>
</html>